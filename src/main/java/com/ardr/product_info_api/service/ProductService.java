package com.ardr.product_info_api.service;


import com.ardr.product_info_api.entity.Product;
import com.ardr.product_info_api.model.ProductCreatRequest;
import com.ardr.product_info_api.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public void setProduct(ProductCreatRequest request){
        Product addData = new Product();
        addData.setProductImg(request.getProductImg());
        addData.setProductName(request.getProductName());
        addData.setProductPrice(request.getProductPrice());

       productRepository.save(addData);
    }
}
