package com.ardr.product_info_api.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

@Entity
@Getter
@Setter
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String productImg;

    @Column(nullable = false)
    private String productName;

    @Column(nullable = false)
    private Double productPrice;


}
