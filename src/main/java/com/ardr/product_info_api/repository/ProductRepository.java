package com.ardr.product_info_api.repository;

import com.ardr.product_info_api.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Long, Product> {
}
