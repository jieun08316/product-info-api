package com.ardr.product_info_api.configure;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "Product App",
                description = "product app api",
                version = "v1"))
@RequiredArgsConstructor
@Configuration
public class SwaggerConfig {

    @Bean
    public GroupedOpenApi healthOpenApi() {

        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("상품리스트 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
