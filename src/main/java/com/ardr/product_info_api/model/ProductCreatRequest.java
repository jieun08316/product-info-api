package com.ardr.product_info_api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCreatRequest {

    private String productImg;

    private String productName;

    private Double productPrice;


}
